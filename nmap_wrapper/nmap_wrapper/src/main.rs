use std::process::{Command, Stdio};


fn main() {

    let output = Command::new("nmap")
            .args(["-A", "-T4", "-p", "3389", "-oX", "output.xml", "127.0.0.1"])
            .spawn()
            .expect("Nmap failed to run.");
    }
