use windows::{
    core::*, Win32::UI::WindowsAndMessaging::*,
};

// Result<()> allows main to return a result similar to a C based language
fn main() -> Result<()> {
    unsafe {
        // Does the pop-up to warn a team they should not have clicked on the .exe. Need to figure out a way to handle team numbers dynamically.
        MessageBoxW(None, w!("Team X has clicked on the malicious .exe. Note there is no actual malicious payload, but the action has been recorded by Red Team."), w!("Got by Red Team!"), MB_ICONWARNING);
    }
    Ok(())
}
