# CCDC RT Helper

## Description
The goal of this project is to create the beginnings of a C2 framework that I can use while red teaming in CCDC. This is mostly here as a learning exercise, and I do not recommend anyone use this in it's current state. 

## Installation
TBD

## Usage
TBD

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## TODO
- Figure out how to make a server for this simple implant to call back to
    - Web interface
    - Users DB
    - Nmap status reporter
    - HTTPS
- Nmap wrapper
- Basically everything

## Authors and acknowledgment
The_Asshat

## License
GPL v3

## Project status
In development whenever I have free time. This is going to be slow, and if other's somehow find this useful, that's awesome. But expect many growing pains as I figure things out here as I do not have a CS background at all. 
